{-# OPTIONS_GHC -Wall #-}

module System.F.Configuration(
  siteTitle
, siteDescription
, siteAuthorName
, siteAuthorEmail
, siteRoot
, systemfEmail
, systemfPhone
, systemfConfiguration
, systemfGitlabio
, feedConfiguration
) where

import Hakyll

siteTitle ::
  String
siteTitle =
  "System F"

siteDescription ::
  String
siteDescription =
  "System F"

siteAuthorName ::
  String
siteAuthorName =
  "Tony Morris"

siteAuthorEmail ::
  String
siteAuthorEmail =
  "systemf@systemf.com.au"

siteRoot ::
  String
siteRoot =
  "https://www.systemf.com.au/"

systemfEmail ::
  String
systemfEmail =
  "systemf@systemf.com.au"

systemfPhone ::
  String
systemfPhone =
  "0489181111"

systemfGitlabio ::
  String
systemfGitlabio =
  "https://system-f.gitlab.io"

feedConfiguration ::
  FeedConfiguration
feedConfiguration =
  FeedConfiguration { feedTitle = siteTitle
  , feedDescription = siteDescription
  , feedAuthorName = siteAuthorName
  , feedAuthorEmail = siteAuthorEmail
  , feedRoot = siteRoot
  }

systemfConfiguration ::
  Configuration
systemfConfiguration =
  defaultConfiguration {
    destinationDirectory = "public"
  , previewHost = "0.0.0.0"
  }
