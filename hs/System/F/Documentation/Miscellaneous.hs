{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation.Miscellaneous where
import System.F.Documentation.Constants ( imgSize )
import System.F.Documentation.Data
import System.F.Documentation.Util

make ::
  String
  -> String
  -> DocumentWithNameUriImage
make x y =
  DocumentWithNameUriImage
    y
    (x <> ".pdf")
    (x <> "_page1.pdf.png-" <> imgSize <> ".png")

makeMisc ::
  String
  -> String
  -> DocumentWithNameUriImage
makeMisc x =
  make ("https://miscellaneous.documentation.systemf.com.au/" <> x)

miscellaneous ::
  Documents DocumentWithNameUriImage
miscellaneous =
  htmlEscapeDocuments $
  Documents
    "Miscellaneous"
    "miscellaneous"
    [
      makeMisc
        "flight-notification-form"
        "Flight Notification Form (Navigation Log)"
    , makeMisc
        "fuel-plan"
        "Fuel Plan"
    , makeMisc
        "fuel-log"
        "Fuel Log"
    , makeMisc
        "radio-navigation-log"
        "Radio Navigation Log"
    , makeMisc
        "orchid-beach"
        "Orchid Beach (YORC)"
    , make
        "https://system-f.gitlab.io/aircraft/diagrams/cessna-182p/endurance-profile_figure-5-9"
        "C182P Endurance"
    , makeMisc
        "se_pist-questionnaire"
        "Single Engine Piston Aeroplane Endorsement"
    , makeMisc
        "danger-areas"
        "Danger Areas Log"
    ]
