{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE LambdaCase #-}

module System.F.Documentation.Util where

import Data.Bool ( bool )
import Data.Char ( isSpace )
import System.F.Documentation.Constants ( imgSize )
import System.F.Documentation.Data
import System.FilePath ( (</>) )

npages ::
  String
  -> IO String
npages s =
  let unspace =
        dropWhile isSpace
      trim =
        reverse . unspace . reverse . unspace
      pagesWord q =
        bool "pages" "page" (q == "1")
  in  do  p <- readFile ("n-pages" </> s <> ".pdf.npages")
          let p' = trim p
          pure (p' <> " " <> pagesWord p')

npagesDocuments ::
  Traversable t =>
  String
  -> String
  -> t (Documents DocumentWithNameUri)
  -> IO (t (Documents DocumentWithNameUriImageNPages))
npagesDocuments u npage docs =
  let perDocument (DocumentWithNameUri t s) =
        do  p <- npages (npage <> s)
            pure (DocumentWithNameUriImageNPages t (u <> s <> ".pdf") (u <> s <> "_page1.pdf-" <> imgSize <> ".png") p)
  in  traverse (\(Documents n i m) -> Documents n i <$> traverse perDocument m) docs

htmlEscape ::
  String
  -> String
htmlEscape s =
  s >>= \case
    '—' -> "&mdash;"
    '&' -> "&amp;"
    '<' -> "&lt;"
    c -> [c]

htmlEscapeDocumentName ::
  HasDocumentName t =>
  t
  -> t
htmlEscapeDocumentName =
  over documentName htmlEscape

htmlEscapeDocuments ::
  (HasDocumentsValues a x, HasDocumentName x) =>
  a
  -> a
htmlEscapeDocuments =
  over (documentsValues . mapped . documentName) htmlEscape

htmlEscapeDocumentsList ::
  (Functor f, HasDocumentsValues a x, HasDocumentName x) =>
  f a
  -> f a
htmlEscapeDocumentsList =
  fmap htmlEscapeDocuments
