{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation.RAAus where

import System.F.Documentation.Data
import System.F.Documentation.Util

raaus ::
  IO [Documents DocumentWithNameUriImageNPages]
raaus =
  npagesDocuments "https://raaus-documentation.systemf.com.au/" "raaus-documentation/" raaus'

raaus' ::
  [Documents DocumentWithNameUri]
raaus' =
  htmlEscapeDocumentsList
  [
    Documents
      "RAAus Operations Manual (Issue 7.1.2 17 July 2024)"
      "RAAus-Flight-Operations-Manual-issue712"
      [
        DocumentWithNameUri "Complete" "RAAus-Flight-Operations-Manual-issue712"
      , DocumentWithNameUri "Table of Contents" "RAAus-Flight-Operations-Manual-issue712_table-of-contents"
      , DocumentWithNameUri "2.01 Pilot Certificate — Group A and B" "RAAus-Flight-Operations-Manual-issue712_2.01-pilot-certificate-group-a-and-b"
      , DocumentWithNameUri "2.04 Pilot Certificates — Aeroplane Groups" "RAAus-Flight-Operations-Manual-issue712_2.04-pilot-certificates-aeroplane-groups"
      , DocumentWithNameUri "2.04 Pilot Certificates — Pilot Qualifications" "RAAus-Flight-Operations-Manual-issue712_2.04-pilot-qualifications"
      , DocumentWithNameUri "2.06 Student or Converting Pilot Certificate" "RAAus-Flight-Operations-Manual-issue712_2.06-student-or-converting-pilot-certificate"
      , DocumentWithNameUri "2.07 Pilot Certificate — Group A and B" "RAAus-Flight-Operations-Manual-issue712_2.07-pilot-certificate-group-a-and-b"
      , DocumentWithNameUri "2.08 Instructor Rating — Group A and B" "RAAus-Flight-Operations-Manual-issue712_2.08-instructor-rating-group-a-and-b"
      , DocumentWithNameUri "2.09 Senior Instructor Rating — Group A and B" "RAAus-Flight-Operations-Manual-issue712_2.09-senior-instructor-rating-group-a-and-b"
      , DocumentWithNameUri "2.10 Chief Flying Instructor / Deputy Chief Flying Instructor Approval — Group A and B" "RAAus-Flight-Operations-Manual-issue712_2.10-cfi-approval-group-a-and-b"
      , DocumentWithNameUri "Version 7.1.1 vs 7.1.2 Difference" "raaus-operations-manual-711-712-diff"
      ]
  , Documents
      "RAAus Syllabus of Flight Training (Issue 7 October 2014)"
      "raaus-syllabus-flight-training-7"
      [
        DocumentWithNameUri "Complete" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1"
      , DocumentWithNameUri "Table of Contents" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_table-of-contents"
      , DocumentWithNameUri "Group A (3-axis) Syllabus 1.01" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_1.01-group-a-3-axis-syllabus"
      , DocumentWithNameUri "Passenger Carrying Endorsement Syllabus 1.04" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_1.04-passenger-carrying-endorsement-syllabus"
      , DocumentWithNameUri "Cross Country Endorsement Syllabus 1.05" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_1.05-cross-country-endorsement-syllabus"
      , DocumentWithNameUri "Formation Endorsement Syllabus 1.06" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_1.06-formation-endorsement-syllabus"
      , DocumentWithNameUri "Low Level Endorsement Syllabus 1.08" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_1.08-low-level-endorsement-syllabus"
      , DocumentWithNameUri "Basic Aeronautical Knowledge Syllabus 2.01" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_2.01-basic-aeronautical-knowledge-syllabus"
      , DocumentWithNameUri "Air Legislation Syllabus 2.02" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_2.02-air-legislation-syllabus"
      , DocumentWithNameUri "Navigation and Meteorology Syllabus 2.03" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_2.03-navigation-and-meteorology-syllabus"
      , DocumentWithNameUri "Radio Operator Syllabus 2.04" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_2.04-radio-operator-syllabus"
      , DocumentWithNameUri "Human Factors Syllabus 2.05" "1-syllabus-of-flight-training-issue-7-v2-single-pages-1_2.05-human-factors-syllabus"
      ]
  , Documents
      "RAAus Technical Manual (Issue 4.3 02 December 2024)"
      "raaus-technical-manual-4.3"
      [
        DocumentWithNameUri "Complete" "raaus-technical-manual-issue-43"
      , DocumentWithNameUri "Table of Contents" "raaus-technical-manual-issue-43_table-of-contents"
      , DocumentWithNameUri "Abbreviations and Definitions" "raaus-technical-manual-issue-43_abbreviations-and-definitions"
      , DocumentWithNameUri "5.1 Aircraft Registration" "raaus-technical-manual-issue-43_5.1-aircraft-registration"
      , DocumentWithNameUri "12.1 Daily and Pre-flight Inspections" "raaus-technical-manual-issue-43_12.1-daily-and-pre-flight-inspections"
      , DocumentWithNameUri "12.7 Pilot Maintenance" "raaus-technical-manual-issue-43_12.7-pilot_maintenance"
      , DocumentWithNameUri "12.8 Annual and Periodic Maintenance" "raaus-technical-manual-issue-43_12.8-annual_and_periodic_maintenance"
      , DocumentWithNameUri "13.1 Defect Reporting and Airworthiness Notices" "raaus-technical-manual-issue-43_13.1-defect_reporting_and_airworthiness_notices"
      , DocumentWithNameUri "13.2 Immediately Reportable Matter (IRM) and Routine Reportable Matter - TSI Act (2003)" "raaus-technical-manual-issue-43_13.2-immediately_reportable_and_routine_reportable_matters"
      , DocumentWithNameUri "Version 4.1 vs 4.3 Difference" "raaus-technical-manual-41-43-diff"
      ]
  ]
