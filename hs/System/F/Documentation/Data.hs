{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation.Data(
  module D
) where


import Control.Lens as D hiding ( Context, to, ix )
import System.F.Documentation.Data.Documents as D
import System.F.Documentation.Data.DocumentWithNameUri as D
import System.F.Documentation.Data.DocumentWithNameUriImage as D
import System.F.Documentation.Data.DocumentWithNameUriImageNPages as D
import System.F.Documentation.Data.DocumentWithNameUriNPages as D
import System.F.Documentation.Data.HasDocumentImage as D
import System.F.Documentation.Data.HasDocumentName as D
import System.F.Documentation.Data.HasDocumentNPages as D
import System.F.Documentation.Data.HasDocumentUri as D
import System.F.Documentation.Data.HasDocumentsId as D
import System.F.Documentation.Data.HasDocumentsName as D
import System.F.Documentation.Data.HasDocumentsValues as D
