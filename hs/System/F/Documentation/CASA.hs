{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation.CASA where

import System.F.Documentation.Data
import System.F.Documentation.Util

casa ::
  IO [Documents DocumentWithNameUriImageNPages]
casa =
  do  c <- npagesDocuments "https://casa-documentation.systemf.com.au/" "casa-documentation/" casa'
      k <- npagesDocuments "https://airport-documentation.systemf.com.au/" "airport-documentation/" stayOnTrack'
      pure (c <> k)

casa' ::
  [Documents DocumentWithNameUri]
casa' =
  htmlEscapeDocumentsList
  [
    Documents
      "Visual Flight Rules Guide"
      "visual-flight-rules-guide"
      $ over (mapped . documentUri) ("visual-flight-rules-guide" <>)
        [
          DocumentWithNameUri "Complete" ""
        , DocumentWithNameUri "Cover" "_cover"
        , DocumentWithNameUri "Quick Menu" "_quick_menu"
        , DocumentWithNameUri "Table of Contents" "_table_of_contents"
        , DocumentWithNameUri "About this guide" "_about_this_guide"
        , DocumentWithNameUri "What is included in this guide?" "_what_is_included_in_this_guide"
        , DocumentWithNameUri "Chapter 1: Know Your Rules and Responsibilities" "_chapter1"
        , DocumentWithNameUri "Chapter 2: Planning Your Flight" "_chapter2"
        , DocumentWithNameUri "Chapter 3: Flying Your Aircraft" "_chapter3"
        , DocumentWithNameUri "Chapter 4: Flying Your Helicopter" "_chapter4"
        , DocumentWithNameUri "Chapter 5: Radio Communication Procedures" "_chapter5"
        , DocumentWithNameUri "Chapter 6: Decision-Making and Hazards" "_chapter6"
        , DocumentWithNameUri "Chapter 7: Dealing With Emergency Situations" "_chapter7"
        , DocumentWithNameUri "Appendix: Abbreviations and Acronyms, Definitions" "_appendix_abbreviations_acronyms_definitions"
        , DocumentWithNameUri "Appendix: Are you safe to fly?" "_appendix_are_you_safe_to_fly"
        , DocumentWithNameUri "Appendix: Aircraft Specifications" "_appendix_aircraft_specifications"
        , DocumentWithNameUri "Appendix: Forced Landing initial action" "_appendix_forced_landing_initial_action"
        , DocumentWithNameUri "Appendix: Forced Landing procedure" "_appendix_forced_landing_procedure"
        , DocumentWithNameUri "Appendix: Night VFR checklist" "_appendix_night_vfr_checklist"
        , DocumentWithNameUri "Appendix: Light signals" "_appendix_light_signals"
        , DocumentWithNameUri "Appendix: Signals for the control of aerodrome traffic" "_appendix_signals_for_the_control_of_aerodrome_traffic"
        , DocumentWithNameUri "Appendix: Windsock interpretation" "_appendix_windsock_interpretation"
        , DocumentWithNameUri "Appendix: Conversions" "_appendix_conversions"
        , DocumentWithNameUri "Appendix: Index" "_appendix_index"
        ]
  , Documents
      "CASR Part 61 Aeroplane Category Flight Crew Licensing Plain English Guide"
      "part-61-guide-aeroplane-category-flight-crew-licensing"
      $ over (mapped . documentUri) ("part-61-guide-aeroplane-category-flight-crew-licensing" <>)
      [
        DocumentWithNameUri "Complete" ""
      , DocumentWithNameUri "Cover" "_cover"
      , DocumentWithNameUri "Quick Guide" "_quick_guide"
      , DocumentWithNameUri "Who is this Guide for?" "_who_is_the_guide_for"
      , DocumentWithNameUri "How to use this Guide?" "_how_to_use_this_guide"
      , DocumentWithNameUri "Table of Contents" "_table_of_contents"
      , DocumentWithNameUri "Chapter 1: Understanding the Purposes, Functions and Key Concepts of Licensing" "_chapter1"
      , DocumentWithNameUri "Chapter 2: General Requirements for Gaining a Pilot Licence, Rating or Endorsement" "_chapter2"
      , DocumentWithNameUri "Chapter 3: Student Pilots" "_chapter3"
      , DocumentWithNameUri "Chapter 4: Pilot Licences and Required Ratings and Endorsements" "_chapter4"
      , DocumentWithNameUri "Chapter 5: Aircraft Category, Class and Type Ratings" "_chapter5"
      , DocumentWithNameUri "Chapter 6: Design Feature Endorsements" "_chapter6"
      , DocumentWithNameUri "Chapter 7: Low-Level Rating and Associated Endorsements" "_chapter7"
      , DocumentWithNameUri "Chapter 8: Aerial Application Rating and Associated Endorsements" "_chapter8"
      , DocumentWithNameUri "Chapter 9: Flight Activity Endorsements" "_chapter9"
      , DocumentWithNameUri "Chapter 10: Instrument Ratings" "_chapter10"
      , DocumentWithNameUri "Chapter 11: Pilot Instructor Ratings" "_chapter11"
      , DocumentWithNameUri "Chapter 12: Flight Examiner Rating and Associated Endorsements" "_chapter12"
      , DocumentWithNameUri "Chapter 13: Transitioning a Part 5 of CAR Licence" "_chapter13"
      , DocumentWithNameUri "Chapter 14: Converting a Helicopter Category Licence to an Aeroplane Category Licence" "_chapter14"
      , DocumentWithNameUri "Chapter 15: Recognition of Pilot Certificates Granted by Sport Aviation Bodies" "_chapter15"
      , DocumentWithNameUri "Chapter 16: Recognition of Overseas Authorisations, Licences and Proficiency Checks" "_chapter16"
      , DocumentWithNameUri "Chapter 17: Bilateral Agreements" "_chapter17"
      , DocumentWithNameUri "Chapter 18: Recognition of Australian Defence Force Qualifications" "_chapter18"
      , DocumentWithNameUri "Appendix A: Acronyms and abbreviations" "_appendixa"
      , DocumentWithNameUri "Appendix B: Definitions" "_appendixb"
      , DocumentWithNameUri "Appendix C: Prescribed aircraft and type ratings" "_appendixc"
      , DocumentWithNameUri "Appendix D: Part 61 Manual of standards references" "_appendixd"
      , DocumentWithNameUri "Appendix E: Part 61 Exemptions included in this guide" "_appendixe"
      , DocumentWithNameUri "Appendix F: CASA EX32/24 Flight Crew Licensing and Other Matters (Miscellaneous Exemptions) Instrument 2024 Part 8 – Multi-crew cooperation training" "_appendixf"
      , DocumentWithNameUri "Appendix G: Forms" "_appendixg"
      ]
  , Documents
      "CASR Part 91 General Operating and Flight Rules Plain English Guide"
      "plain-english-guide-part-91-new-flight-operations-regulations-print-version"
      $ over (mapped . documentUri) ("plain-english-guide-part-91-new-flight-operations-regulations-print-version" <>)
        [
          DocumentWithNameUri "Complete" ""
        , DocumentWithNameUri "Cover" "_cover"
        , DocumentWithNameUri "About this guide" "_about_this_guide"
        , DocumentWithNameUri "How to use this guide" "_how_to_use_this_guide"
        , DocumentWithNameUri "Table of Contents" "_table_of_contents"
        , DocumentWithNameUri "Preliminary Rules" "_preliminary_rules"
        , DocumentWithNameUri "General Rules" "_general_rules"
        , DocumentWithNameUri "Operational Rules" "_operational_rules"
        , DocumentWithNameUri "Administrative Rules" "_administrative_rules"
        , DocumentWithNameUri "Appendix A: MOS Chapter 26 − Equipment" "_appendixa"
        , DocumentWithNameUri "Appendix B: Night Vision Imaging Systems flights" "_appendixb"
        , DocumentWithNameUri "Appendix C: Air displays, practice flights for air displays and aerobatic activities in the vicinity of non-controlled aerodromes - exemptions and directions" "_appendixc"
        , DocumentWithNameUri "Appendix D: Observers who may be on certain flight tests and proficiency check flights (EX81/21 Part 4)" "_appendixd"
        , DocumentWithNameUri "Appendix E: Abbreviations and acronyms" "_appendixe"
        , DocumentWithNameUri "Appendix F: Definitions" "_appendixf"
        , DocumentWithNameUri "Appendix G: Regulations - page references" "_appendixg"
        , DocumentWithNameUri "Index" "_index"
        ]
  , Documents
      "CAO 48.1 Fatigue Management Plain English Guide"
      "plain-english-guide-for-fatigue-management-rules-print-version"
      $ over (mapped . documentUri) ("plain-english-guide-for-fatigue-management-rules-print-version" <>)
        [
          DocumentWithNameUri "Complete" ""
        , DocumentWithNameUri "Cover" "_cover"
        , DocumentWithNameUri "About this guide" "_about_this_guide"
        , DocumentWithNameUri "How to use this guide" "_how_to_use_this_guide"
        , DocumentWithNameUri "Table of Contents" "_table_of_contents"
        , DocumentWithNameUri "Legislation" "_legislation"
        , DocumentWithNameUri "Appendices" "_appendices"
        , DocumentWithNameUri "Appendix 1: Basic Limits" "_appendix1"
        , DocumentWithNameUri "Appendix 2: Multi-Pilot Operations Except Flight Training" "_appendix2"
        , DocumentWithNameUri "Appendix 3: Multi-Pilot Operations Except Complex Operations and Flight Training" "_appendix3"
        , DocumentWithNameUri "Appendix 4: Any Operation" "_appendix4"
        , DocumentWithNameUri "Appendix 4A: Balloons" "_appendix4a"
        , DocumentWithNameUri "Appendix 4B: Medical Transport and Emergency Service Operations" "_appendix4b"
        , DocumentWithNameUri "Appendix 5: Aerial Work Operations and Flight Training Associated with Aerial Work" "_appendix5"
        , DocumentWithNameUri "Appendix 5A: Daylight Aerial Work Operations and Flight Training Associated with Aerial Work" "_appendix5a"
        , DocumentWithNameUri "Appendix 6: Flight Training" "_appendix6"
        , DocumentWithNameUri "Appendix 7: FRMS (Fatigue Risk Management System)" "_appendix7"
        , DocumentWithNameUri "Flight/duty and other time limitations" "_flight_duty_time_limitations"
        , DocumentWithNameUri "Definitions" "_definitions"
        ]
  , Documents
      "CASR Part 101 Micro and Excluded RPA Operations Plain English Guide"
      "part-101-micro-excluded-rpa-operations-plain-english-guide"
      $ over (mapped . documentUri) ("part-101-micro-excluded-rpa-operations-plain-english-guide" <>)
        [
          DocumentWithNameUri "Complete" ""
        , DocumentWithNameUri "Cover" "_cover"
        , DocumentWithNameUri "Terminology" "_terminology"
        , DocumentWithNameUri "Table of Contents" "_table_of_contents"
        , DocumentWithNameUri "Chapter 1: Introduction" "_chapter1"
        , DocumentWithNameUri "Chapter 2: Before the First Flight" "_chapter2"
        , DocumentWithNameUri "Chapter 3: Before Every Flight" "_chapter3"
        , DocumentWithNameUri "Chapter 4: During Every Flight" "_chapter4"
        , DocumentWithNameUri "Chapter 5: Enforcement Provisions" "_chapter5"
        , DocumentWithNameUri "Chapter 6: Other Relevant Considerations" "_chapter6"
        , DocumentWithNameUri "Appendices and References" "_appendices_and_references"
        ]
  ]

stayOnTrack' ::
  [Documents DocumentWithNameUri]
stayOnTrack' =
  [
    Documents
      "Stay OnTrack: Flying the Brisbane Region"
      "stay-ontrack-flying-the-brisbane-region"
      [
        DocumentWithNameUri "Stay OnTrack: Flying the Brisbane Region" "stay-ontrack-flying-the-brisbane-region"
      ]
  ]
