{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation.AircraftChecklist where

import System.F.Documentation.Data
import System.F.Documentation.Util

aircraftChecklist ::
  Documents DocumentWithNameUri
aircraftChecklist =
  htmlEscapeDocuments .
  Documents
    "Aircraft Checklists"
    "aircraft-checklist"
    $ over (mapped . documentUri)
    (\z -> "https://miscellaneous.documentation.systemf.com.au/" <> z <> ".pdf")
    aircraftChecklist'

aircraftChecklist' ::
  [DocumentWithNameUri]
aircraftChecklist' =
  [
    DocumentWithNameUri "Aquila A210" "aquila-a210"
  , DocumentWithNameUri "Cessna 152" "cessna-152"
  , DocumentWithNameUri "Cessna 172R" "cessna-172r"
  , DocumentWithNameUri "Cessna 182P" "cessna-182p"
  , DocumentWithNameUri "Eurofox 3K" "eurofox-3k"
  , DocumentWithNameUri "Foxbat A22LS" "foxbat-a22ls"
  , DocumentWithNameUri "Super Decathlon 8KCAB" "super-decathlon"
  , DocumentWithNameUri "Vans RV-14" "vans-rv14"
  ]
