{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation.Software where

import System.F.Documentation.Data
import System.F.Documentation.Util

software ::
  [Documents DocumentWithNameUri]
software =
  htmlEscapeDocumentsList
  [
    Documents
      "Online Practice Drills"
      "practice-drills"
      [
        DocumentWithNameUri "Navigation 1 in 60 Rule" "/p/software/practice/navigation-1in60/"
      , DocumentWithNameUri "Pressure Altitude from QNH and Elevation" "/p/software/practice/pressure-altitude/"
      , DocumentWithNameUri "Density Altitude from Pressure Altitude and Temperature" "/p/software/practice/density-altitude/"
      ]
  , Documents
      "Stratux ADS-B Receiver"
      "stratux-receiver"
      [
        DocumentWithNameUri "stratux-types: Stratux Data Types" "https://hackage.haskell.org/package/stratux-types"
      , DocumentWithNameUri "stratux-websockets: Network functions to Stratux websockets" "https://hackage.haskell.org/package/stratux-websockets"
      , DocumentWithNameUri "stratux-http: Network functions to Stratux HTTP" "https://hackage.haskell.org/package/stratux-http"
      , DocumentWithNameUri "stratux-demo: Demo application to the Stratux ADS-B Receiver, Gyrometer and GPS" "https://hackage.haskell.org/package/stratux-demo"
      ]
  , Documents
      "Libraries"
      "software-libraries"
      [
        DocumentWithNameUri "metar: A network library to obtain METAR/TAF data from the Bureau of Meterology" "https://hackage.haskell.org/package/metar"
      , DocumentWithNameUri "metar-http: A standalone HTTP server for serving METAR/TAF data" "https://hackage.haskell.org/package/metar-http"
      , DocumentWithNameUri "sixfiguregroup: A type-safe library for a Six Figure Group (see AIP)" "https://hackage.haskell.org/package/sixfiguregroup"
      , DocumentWithNameUri "casa-abbreviations-and-acronyms: A standalone executable for fuzzy search through CASA abbreviations and acronyms" "https://hackage.haskell.org/package/casa-abbreviations-and-acronyms"
      , DocumentWithNameUri "aip-version: A standalone executable for returning the dates of various current AIP documents" "https://hackage.haskell.org/package/aip-version"
      , DocumentWithNameUri "aviation-cessna172-weight-balance: A library for accepting the weight parameters to a Cessna 172, and outputs a PDF containing the loading envelope" "https://hackage.haskell.org/package/aviation-cessna172-weight-balance"
      ]
  ]
