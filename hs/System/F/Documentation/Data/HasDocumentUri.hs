{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.HasDocumentUri where

import Control.Lens ( Lens' )

class HasDocumentUri a where
  documentUri ::
    Lens' a String

instance HasDocumentUri String where
  documentUri =
    id
