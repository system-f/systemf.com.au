{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.DocumentWithNameUriImageNPages where

import System.F.Documentation.Data.HasDocumentName
    ( HasDocumentName(..) )
import System.F.Documentation.Data.HasDocumentImage
    ( HasDocumentImage(..) )
import System.F.Documentation.Data.HasDocumentNPages
    ( HasDocumentNPages(..) )
import System.F.Documentation.Data.HasDocumentUri
    ( HasDocumentUri(..) )

data DocumentWithNameUriImageNPages =
  DocumentWithNameUriImageNPages
    String
    String
    String
    String
  deriving (Eq, Ord, Show)

instance HasDocumentName DocumentWithNameUriImageNPages where
  documentName f (DocumentWithNameUriImageNPages n u i p) =
    fmap (\n' -> DocumentWithNameUriImageNPages n' u i p) (f n)

instance HasDocumentUri DocumentWithNameUriImageNPages where
  documentUri f (DocumentWithNameUriImageNPages n u i p) =
    fmap (\u' -> DocumentWithNameUriImageNPages n u' i p) (f u)

instance HasDocumentImage DocumentWithNameUriImageNPages where
  documentImage f (DocumentWithNameUriImageNPages n u i p) =
    fmap (\i' -> DocumentWithNameUriImageNPages n u i' p) (f i)

instance HasDocumentNPages DocumentWithNameUriImageNPages where
  documentNPages f (DocumentWithNameUriImageNPages n u i p) =
    fmap (DocumentWithNameUriImageNPages n u i) (f p)
