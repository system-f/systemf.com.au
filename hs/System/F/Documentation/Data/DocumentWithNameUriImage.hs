{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.DocumentWithNameUriImage where

import System.F.Documentation.Data.HasDocumentImage
    ( HasDocumentImage(..) )
import System.F.Documentation.Data.HasDocumentName
    ( HasDocumentName(..) )
import System.F.Documentation.Data.HasDocumentUri
    ( HasDocumentUri(..) )

data DocumentWithNameUriImage =
  DocumentWithNameUriImage
    String
    String
    String
  deriving (Eq, Ord, Show)

instance HasDocumentName DocumentWithNameUriImage where
  documentName f (DocumentWithNameUriImage n u i) =
    fmap (\n' -> DocumentWithNameUriImage n' u i) (f n)

instance HasDocumentUri DocumentWithNameUriImage where
  documentUri f (DocumentWithNameUriImage n u i) =
    fmap (\u' -> DocumentWithNameUriImage n u' i) (f u)

instance HasDocumentImage DocumentWithNameUriImage where
  documentImage f (DocumentWithNameUriImage n u i) =
    fmap (DocumentWithNameUriImage n u) (f i)
