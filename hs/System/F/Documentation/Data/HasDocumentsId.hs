{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.HasDocumentsId where

import Control.Lens ( Lens' )

class HasDocumentsId a where
  documentsId ::
    Lens' a String

instance HasDocumentsId String where
  documentsId =
    id
