{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}

module System.F.Documentation.Data.HasDocumentsValues where

import Control.Lens ( Lens' )

class HasDocumentsValues a x | a -> x where
  documentsValues ::
    Lens' a [x]

instance HasDocumentsValues [x] x where
  documentsValues =
    id
