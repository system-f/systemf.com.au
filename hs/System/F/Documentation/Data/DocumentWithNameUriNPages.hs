{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.DocumentWithNameUriNPages where

import System.F.Documentation.Data.HasDocumentName
    ( HasDocumentName(..) )
import System.F.Documentation.Data.HasDocumentNPages
    ( HasDocumentNPages(..) )
import System.F.Documentation.Data.HasDocumentUri
    ( HasDocumentUri(..) )

data DocumentWithNameUriNPages =
  DocumentWithNameUriNPages
    String
    String
    String
  deriving (Eq, Ord, Show)

instance HasDocumentName DocumentWithNameUriNPages where
  documentName f (DocumentWithNameUriNPages n u p) =
    fmap (\n' -> DocumentWithNameUriNPages n' u p) (f n)

instance HasDocumentUri DocumentWithNameUriNPages where
  documentUri f (DocumentWithNameUriNPages n u p) =
    fmap (\u' -> DocumentWithNameUriNPages n u' p) (f u)

instance HasDocumentNPages DocumentWithNameUriNPages where
  documentNPages f (DocumentWithNameUriNPages n u p) =
    fmap (DocumentWithNameUriNPages n u) (f p)
