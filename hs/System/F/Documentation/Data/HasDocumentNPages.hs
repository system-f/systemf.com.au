{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.HasDocumentNPages where

import Control.Lens ( Lens' )

class HasDocumentNPages a where
  documentNPages ::
    Lens' a String

instance HasDocumentNPages String where
  documentNPages =
    id
