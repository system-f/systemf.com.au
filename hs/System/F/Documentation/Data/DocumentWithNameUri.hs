{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.DocumentWithNameUri where

import System.F.Documentation.Data.HasDocumentName
    ( HasDocumentName(..) )
import System.F.Documentation.Data.HasDocumentUri
    ( HasDocumentUri(..) )

data DocumentWithNameUri =
  DocumentWithNameUri
    String
    String
  deriving (Eq, Ord, Show)

instance HasDocumentName DocumentWithNameUri where
  documentName f (DocumentWithNameUri n u) =
    fmap (`DocumentWithNameUri` u) (f n)

instance HasDocumentUri DocumentWithNameUri where
  documentUri f (DocumentWithNameUri n u) =
    fmap (DocumentWithNameUri n) (f u)
