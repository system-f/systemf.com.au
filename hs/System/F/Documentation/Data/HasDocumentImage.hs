{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.HasDocumentImage where

import Control.Lens ( Lens' )

class HasDocumentImage a where
  documentImage ::
    Lens' a String

instance HasDocumentImage String where
  documentImage =
    id
