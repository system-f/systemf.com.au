{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module System.F.Documentation.Data.Documents where

import System.F.Documentation.Data.HasDocumentsId
import System.F.Documentation.Data.HasDocumentsName
import System.F.Documentation.Data.HasDocumentsValues

data Documents a =
  Documents
    String
    String
    [a]
  deriving (Eq, Ord, Show)

instance Functor Documents where
  fmap f (Documents n i ds) =
    Documents n i (fmap f ds)

instance HasDocumentsName (Documents a) where
  documentsName f (Documents n i ds) =
    fmap (\n' -> Documents n' i ds) (f n)

instance HasDocumentsId (Documents a) where
  documentsId f (Documents n i ds) =
    fmap (\i' -> Documents n i' ds) (f i)

instance HasDocumentsValues (Documents a) a where
  documentsValues f (Documents n i ds) =
    fmap (Documents n i) (f ds)
