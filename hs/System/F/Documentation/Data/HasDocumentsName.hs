{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.HasDocumentsName where

import Control.Lens ( Lens' )

class HasDocumentsName a where
  documentsName ::
    Lens' a String

instance HasDocumentsName String where
  documentsName =
    id
