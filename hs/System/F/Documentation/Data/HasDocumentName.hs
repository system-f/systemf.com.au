{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

module System.F.Documentation.Data.HasDocumentName where

import Control.Lens ( Lens' )

class HasDocumentName a where
  documentName ::
    Lens' a String

instance HasDocumentName String where
  documentName =
    id
