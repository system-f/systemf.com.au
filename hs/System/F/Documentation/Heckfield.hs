{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation.Heckfield where

import System.F.Documentation.Constants ( imgSize )
import System.F.Documentation.Data
import System.F.Documentation.Util

heckfield ::
  String
  -> String
  -> [Documents DocumentWithNameUriImage]
heckfield ersa charts =
  htmlEscapeDocumentsList
  [
    Documents
      "ERSA Heckfield (YHEC)"
      "ersa-heckfield"
      [
        DocumentWithNameUriImage "Facilities"  ("https://www.airservicesaustralia.com/aip/current/ersa/FAC_YHEC_" <> ersa <> ".pdf")("https://airport-documentation.systemf.com.au/ersa-FAC_YHEC_page1.pdf-" <> imgSize <> ".png")
      ]
  , Documents
      "Gold Coast Diagram"
      "gold-coast-diagram"
      [
        DocumentWithNameUriImage "Gold Coast Chart extract from ERSA" "https://airport-documentation.systemf.com.au/ybcg-chart-02-dec-2021.pdf" "https://airport-documentation.systemf.com.au/ybcg-chart-02-dec-2021_page1.pdf.png"
      ]
  , Documents
      "Aeronautical Charts"
      "archerfield-aeronautical-charts"
      [
        DocumentWithNameUriImage "Brisbane Visual Navigation Chart (VNC)" ("https://www.airservicesaustralia.com/aip/current/aipchart/vnc/Brisbane_VNC_" <> charts <> ".pdf") ("https://airport-documentation.systemf.com.au/Brisbane_VNC.pdf-" <> imgSize <> ".png")
      , DocumentWithNameUriImage "Brisbane/Sunshine Coast Visual Terminal Chart (VTC)" ("https://www.airservicesaustralia.com/aip/current/aipchart/vtc/Brisbane_Sunshine_VTC_" <> charts <> ".pdf") ("https://airport-documentation.systemf.com.au/Brisbane_Sunshine_VTC.pdf-" <> imgSize <> ".png")
      ]
  ]
