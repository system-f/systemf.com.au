{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation.Archerfield where

import System.F.Documentation.Constants ( imgSize )
import System.F.Documentation.Data
import System.F.Documentation.Util

archerfield ::
  String
  -> String
  -> [Documents DocumentWithNameUriImage]
archerfield ersa charts =
  htmlEscapeDocumentsList
  [
    Documents
      "ERSA Archerfield (YBAF)"
      "ersa-archerfield"
      [
        DocumentWithNameUriImage "Facilities" ("https://www.airservicesaustralia.com/aip/current/ersa/FAC_YBAF_" <> ersa <> ".pdf") ("https://airport-documentation.systemf.com.au/ersa-FAC_YBAF_page1.pdf-" <> imgSize <> ".png")
      , DocumentWithNameUriImage "Runway Distances" ("https://www.airservicesaustralia.com/aip/current/ersa/RDS_YBAF_" <> ersa <> ".pdf") ("https://airport-documentation.systemf.com.au/ersa-RDS_YBAF_page1.pdf-" <> imgSize <> ".png")
      ]
  , Documents
      "Archerfield Radio Frequencies"
      "archerfield-radio-frequencies"
      [
        DocumentWithNameUriImage "Archerfield Radio Frequencies" "https://airport-documentation.systemf.com.au/ybaf-frequencies.pdf" "https://airport-documentation.systemf.com.au/ybaf-frequencies_page1.pdf.png"
      ]
  , Documents
      "Archerfield Radio Reference"
      "archerfield-radio-reference"
      [
        DocumentWithNameUriImage "Complete" "https://airport-documentation.gitlab.io/radio-reference/ybaf_radio-reference-complete.pdf" ("https://airport-documentation.gitlab.io/radio-reference/ybaf_radio-reference-complete_page1.pdf.png-" <> imgSize <> ".png")
      , DocumentWithNameUriImage "After startup, requesting taxi clearance" "https://airport-documentation.gitlab.io/radio-reference/ybaf_after-start-up.pdf" ("https://airport-documentation.gitlab.io/radio-reference/ybaf_after-start-up_page1.pdf.png-" <> imgSize <> ".png")
      , DocumentWithNameUriImage "Holding, ready for take-off" "https://airport-documentation.gitlab.io/radio-reference/ybaf_holding-ready.pdf" ("https://airport-documentation.gitlab.io/radio-reference/ybaf_holding-ready_page1.pdf.png-" <> imgSize <> ".png")
      , DocumentWithNameUriImage "Target, reporting inbound" "https://airport-documentation.gitlab.io/radio-reference/ybaf_target-inbound.pdf" ("https://airport-documentation.gitlab.io/radio-reference/ybaf_target-inbound_page1.pdf.png-" <> imgSize <> ".png")
      , DocumentWithNameUriImage "After landing, request taxi" "https://airport-documentation.gitlab.io/radio-reference/ybaf_after-landing.pdf" ("https://airport-documentation.gitlab.io/radio-reference/ybaf_after-landing_page1.pdf.png-" <> imgSize <> ".png")
      , DocumentWithNameUriImage "Circuit" "https://airport-documentation.gitlab.io/radio-reference/ybaf_circuit.pdf" ("https://airport-documentation.gitlab.io/radio-reference/ybaf_circuit_page1.pdf.png-" <> imgSize <> ".png")
      ]
  , Documents
      "Archerfield Diagram"
      "archerfield-diagram"
      [
        DocumentWithNameUriImage "Archerfield Diagram extract from ERSA" "https://airport-documentation.systemf.com.au/ybaf-chart-24-mar-2022.pdf" "https://airport-documentation.systemf.com.au/ybaf-chart-24-mar-2022_page1.pdf.png"
      ]
  , Documents
      "Gold Coast Diagram"
      "gold-coast-diagram"
      [
        DocumentWithNameUriImage "Gold Coast Chart extract from ERSA" "https://airport-documentation.systemf.com.au/ybcg-chart-02-dec-2021.pdf" "https://airport-documentation.systemf.com.au/ybcg-chart-02-dec-2021_page1.pdf.png"
      ]
  , Documents
      "Aeronautical Charts"
      "archerfield-aeronautical-charts"
      [
        DocumentWithNameUriImage "Brisbane Visual Navigation Chart (VNC)" ("https://www.airservicesaustralia.com/aip/current/aipchart/vnc/Brisbane_VNC_" <> charts <> ".pdf") ("https://airport-documentation.systemf.com.au/Brisbane_VNC.pdf-" <> imgSize <> ".png")
      , DocumentWithNameUriImage "Brisbane/Sunshine Coast Visual Terminal Chart (VTC)" ("https://www.airservicesaustralia.com/aip/current/aipchart/vtc/Brisbane_Sunshine_VTC_" <> charts <> ".pdf") ("https://airport-documentation.systemf.com.au/Brisbane_Sunshine_VTC.pdf-" <> imgSize <> ".png")
      ]
  , Documents
      "Tips for Flying at Archerfield (Airservices)"
      "tips-for-flying-at-archerfield"
      [
        DocumentWithNameUriImage "Tips for Flying at Archerfield" "https://airport-documentation.systemf.com.au/16-106FAC_Tips-for-flying-at-Archerfield_WEB.pdf" ("https://airport-documentation.systemf.com.au/16-106FAC_Tips-for-flying-at-Archerfield_WEB_page1.pdf-" <> imgSize <> ".png")
      ]

  , Documents
      "Stay OnTrack: Flying the Brisbane Region"
      "stay-ontrack-flying-the-brisbane-region"
      [
        DocumentWithNameUriImage "Stay OnTrack: Flying the Brisbane Region" "https://airport-documentation.systemf.com.au/stay-ontrack-flying-the-brisbane-region.pdf" ("https://airport-documentation.systemf.com.au/stay-ontrack-flying-the-brisbane-region_page1.pdf-" <> imgSize <> ".png")
      ]
  ]
