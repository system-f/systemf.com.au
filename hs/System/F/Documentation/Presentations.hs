{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation.Presentations where

import Control.Lens
import Data.List
import System.F.Documentation.Data
import System.F.Documentation.Util

slidesBase ::
  String
slidesBase =
  "https://presentation-slides.gitlab.io/"

aviationBriefingSlides ::
  [Documents DocumentWithNameUri]
aviationBriefingSlides =
  let briefings =
        [
          DocumentWithNameUri "Briefing: Weight and Balance" "weight-and-balance"
        , DocumentWithNameUri "Briefing: Power Curves" "power-curves"
        , DocumentWithNameUri "Briefing: MPPC" "mppc"
        , DocumentWithNameUri "Briefing: VOR" "vor"
        , DocumentWithNameUri "Briefing: ADF" "adf"
        , DocumentWithNameUri "Briefing: Night VFR Introduction" "night-vfr-introduction"
        , DocumentWithNameUri "Briefing: Night VFR Human Factors" "night-vfr-human-factors"
        , DocumentWithNameUri "Briefing: Night VFR Circuits" "night-vfr-circuits"
        , DocumentWithNameUri "Briefing: Night VFR Navigation" "night-vfr-navigation"
        , DocumentWithNameUri "Briefing: GPS Garmin 430" "gps-garmin-430"
        ]
      perDocument d =
        Documents
          (view documentName d)
          (view documentUri d)
          [DocumentWithNameUri "Slides" (slidesBase <> view documentUri d <> "/")]
  in  fmap perDocument briefings

----

data SlidesPath =
  NoPath
  | BasePath String
  | FullPath String
  deriving (Eq, Show)

slidesPathToMaybe ::
  SlidesPath
  -> Maybe String
slidesPathToMaybe NoPath =
  Nothing
slidesPathToMaybe (BasePath s) =
  Just (slidesBase <> "outputs/" <> s)
slidesPathToMaybe (FullPath s) =
  Just s

data PresentationGiven =
  PresentationGiven
    String -- id
    SlidesPath -- path to slides
    [(String, Maybe String)] -- urls to video
    String -- name
    String -- location
    String -- date
  deriving (Eq, Show)

videoLinkOnly1 ::
  String
  -> [(String, Maybe String)]
videoLinkOnly1 s =
  pure (s, mempty)

videoLink ::
  String
  -> String
  -> (String, Maybe String)
videoLink s v =
  (s, pure v)

data Presentation =
  Presentation
    String -- name
    String -- id
    (Maybe String) -- source
    [PresentationGiven]
  deriving (Eq, Show)

softwarePresentations ::
  [Presentation]
softwarePresentations =
  [
    Presentation
      "The Expression Problem, Generalised"
      "the-expression-problem-generalised"
      (Just "https://gitlab.com/presentation-slides/the-expression-problem-generalised")
      [
        PresentationGiven
          "functional-conf-2025-01-24"
          (FullPath "https://presentation-slides.gitlab.io/the-expression-problem-generalised/")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=CYnqRDYJugw")
          "Functional Conf"
          "Bangalore, India"
          "2025-01-24"
      ]
  , Presentation
      "Type-hole development"
      "type-hole-development"
      (Just "https://github.com/system-f/fp-course/")
      [
        PresentationGiven
          "functional-conf-2022-03-06"
          NoPath
          (videoLinkOnly1 "https://www.youtube.com/watch?v=hhpCVXrhssE")
          "Functional Conf"
          "Bangalore, India"
          "2022-03-26"
      ]
  , Presentation
      "Types and Tests"
      "types-and-tests"
      (Just "https://github.com/tonymorris/types-and-tests/")
      [
        PresentationGiven
          "b453968f2d6db42cf24369b5b66033258c7eb8fd"
          (BasePath "types-and-tests/b453968f2d6db42cf24369b5b66033258c7eb8fd/types-and-tests.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=EUBwDiLjUJQ")
          "Agile India"
          "Bangalore, India"
          "2020-10-16"
      ]
  , Presentation
      "Zippers and Algebra and Stuff"
      "zippers-and-algebra-and-stuff"
      (Just "https://github.com/system-f/zippers/")
      [
        PresentationGiven
          "4a62ca7a4e5e62426b44b426fb0c859de5c65565"
          (BasePath "zippers/4a62ca7a4e5e62426b44b426fb0c859de5c65565/zippers.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=HqHdgBXOOsE")
          "Functional Conf"
          "Bangalore India"
          "2019-11-16"
      , PresentationGiven
          "bd054c210649101b84662c614fc45af3c27a5eef"
          (BasePath "zippers/bd054c210649101b84662c614fc45af3c27a5eef/zippers.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=okHznGNAgbw")
          "LambdaConf"
          "Boulder, Colorado, USA"
          "2019-06-07"
      , PresentationGiven
          "0a1062fd0526d7ac1f41ade1e4db1465d311b4fd"
          (BasePath "zippers/0a1062fd0526d7ac1f41ade1e4db1465d311b4fd/zippers.pdf")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2018-11-13"
      ]
  , Presentation
      "An Intuition for List Folds"
      "an-intuition-for-list-folds"
      (Just "https://github.com/tonymorris/list-folds/")
      [
        PresentationGiven
          "bfpg-2024-09-10"
          NoPath
          (videoLinkOnly1 "https://vimeo.com/1009152406")
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2024-09-10"
      , PresentationGiven
          "4ca721e3c79ae76b931c5384d458d5c638935997"
          (BasePath "list-folds/4ca721e3c79ae76b931c5384d458d5c638935997/list-folds.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=t9pxo7L8mS0")
          "Functional Conf"
          "Bangalore, India"
          "2019-11-15"
      , PresentationGiven
          "feb8749ff8447c9558f733f9493292a824b414ad"
          (BasePath "list-folds/feb8749ff8447c9558f733f9493292a824b414ad/list-folds.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=GPwtT31zKRY")
          "Lambdaconf"
          "Boulder, Colorado, USA"
          "2018-06-07"
      ]
  , Presentation
      "Introduction to Functional Programming"
      "introduction-to-functional-programming"
      (Just "https://github.com/system-f/fp-introduction/")
      [
        PresentationGiven
          "60e6818f30bbfe733ba0527ba729c77450df66c5"
          (BasePath "fp-introduction/60e6818f30bbfe733ba0527ba729c77450df66c5/fp-introduction.pdf")
          mempty
          "DDD Perth"
          "Western Australia"
          "2018-05-01"
      , PresentationGiven
          "d907fb7ef39700d4b2fd975086fcd210e16d8edf"
          (BasePath "fp-introduction/d907fb7ef39700d4b2fd975086fcd210e16d8edf/fp-introduction.pdf")
          mempty
          "Women Who Code"
          "Brisbane, Australia"
          "2018-02-28"
      ]
  , Presentation
      "Introduction to FP Using Haskell (Part 1)"
      "introduction-to-fp-using-haskell-part-1"
      Nothing
      [
        PresentationGiven
          "lambdaconf-2018-06-04"
          NoPath
          (videoLinkOnly1 "https://www.youtube.com/watch?v=K1UjjcdBYp0")
          "LambdaConf"
          "Boulder, Colorado, USA"
          "2018-06-04"
      ]
  , Presentation
      "Introduction to FP Using Haskell (Part 2)"
      "introduction-to-fp-using-haskell-part-2"
      Nothing
      [
        PresentationGiven
          "lambdaconf-2018-06-04"
          NoPath
          (videoLinkOnly1 "https://www.youtube.com/watch?v=Jpt5eLHeZC4")
          "LambdaConf"
          "Boulder, Colorado, USA"
          "2018-06-04"
      ]
  , Presentation
      "Trees That Grow"
      "trees-that-grow"
      (Just "https://github.com/tonymorris/trees-that-grow/")
      [
        PresentationGiven
          "bc4ae902ca1d3bf37af86be8021d64a1de2b5116"
          (BasePath "trees-that-grow/bc4ae902ca1d3bf37af86be8021d64a1de2b5116/trees-that-grow.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=t9pxo7L8mS0")
          "Functional Conf"
          "Bangalore, India"
          "2019-11-15"
      ]
  , Presentation
      "Intro to Functional Programming"
      "intro-to-functional-programming"
      Nothing
      [
        PresentationGiven
          "uqcs-2016-10-20"
          NoPath
          (videoLinkOnly1 "https://www.youtube.com/watch?v=LDzNa77WEXU")
          "UQ Computing Society"
          "Brisbane, Australia"
          "2016-10-20"
      ]
  , Presentation
      "Functional Programming in Aviation"
      "functional-programming-in-aviation"
      (Just "https://github.com/tonymorris/fp-in-aviation/")
      [
        PresentationGiven
          "1dec5542e87e2a74e1b3c3dae4bd60a02e65c216"
          (BasePath "fp-in-aviation/1dec5542e87e2a74e1b3c3dae4bd60a02e65c216/fp-in-aviation.pdf")
          mempty
          "YOW! Conference"
          "Sydney, Australia"
          "2017-12-06"
      , PresentationGiven
          "d97ae118e1f402b5da52fe6605bc0c8f88a46537"
          (BasePath "fp-in-aviation/d97ae118e1f402b5da52fe6605bc0c8f88a46537/fp-in-aviation.pdf")
          mempty
          "YOW! Conference"
          "Brisbane, Australia"
          "2017-12-04"
      , PresentationGiven
          "6e9d75112c41f2c60babdf407dba6881a2daca24"
          (BasePath "fp-in-aviation/6e9d75112c41f2c60babdf407dba6881a2daca24/fp-in-aviation.pdf")
          mempty
          "YOW! Conference"
          "Melbourne, Australia"
          "2017-11-30"
      , PresentationGiven
          "27aea451f77aa67cc5c1324cde0dccdb27ea5d9c"
          (BasePath "fp-in-aviation/27aea451f77aa67cc5c1324cde0dccdb27ea5d9c/fp-in-aviation.pdf")
          mempty
          "YOW! Conference"
          "Bangalore, India"
          "2017-11-18"
      , PresentationGiven
          "compose-2017-08-28"
          NoPath
          (videoLinkOnly1 "https://www.youtube.com/watch?v=ZOII5dbduKU")
          "Compose Conference"
          "Melbourne, Australia"
          "2017-08-28"
      , PresentationGiven
          "de12226d7fb6b2696267faa68edb6df6e629b0a9"
          (BasePath "fp-in-aviation/de12226d7fb6b2696267faa68edb6df6e629b0a9/fp-in-aviation.pdf")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2016-08-09"
      ]
  , Presentation
      "Monad Transformers"
      "monad-transformers"
      (Just "https://github.com/tonymorris/monad-transformers/")
      [
        PresentationGiven
          "8de95b5b9d29c395a68cc8940ca03faad204e474"
          (BasePath "monad-transformers/8de95b5b9d29c395a68cc8940ca03faad204e474/monad-transformers.pdf")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2017-06-13"
      , PresentationGiven
          "cbaa991e0eb49224eb286c1e418e2b9828e1fb21"
          (BasePath "monad-transformers/cbaa991e0eb49224eb286c1e418e2b9828e1fb21/monad-transformers.pdf")
          (videoLinkOnly1 "https://vimeo.com/73648150")
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2013-08-27"
      ]
  , Presentation
      "Let's Lens, 1 day"
      "lets-lens-1-day"
      (Just "https://github.com/system-f/lets-lens/")
      [
        PresentationGiven
          "lambdaconf-2017-05-26"
          NoPath
          [
            videoLink "https://www.youtube.com/watch?v=inyfR3Gb6GE" "Part 1"
          , videoLink "https://www.youtube.com/watch?v=urp4BGvPFSU" "Part 2"
          , videoLink "https://www.youtube.com/watch?v=EWR1nMoQDCM" "Part 3"
          , videoLink "https://www.youtube.com/watch?v=6zvSzZv_uTw" "Part 4"
          , videoLink "https://www.youtube.com/watch?v=UD0T10dzyrU" "Part 5"
          ]
          "Lambdaconf"
          "Boulder, Colorado, USA"
          "2017-05-26"
      ]
  , Presentation
      "Introduction to Functional Programming, 1 day"
      "introduction-to-functional-programming-1-day"
      (Just "https://github.com/system-f/fp-course/")
      [
        PresentationGiven
          "lambdaconf-2017-05-25"
          NoPath
          mempty
          "Lambdaconf"
          "Boulder, Colorado, USA"
          "2017-05-25"
      ]
  , Presentation
      "Functional Programming, Parametricity, Types"
      "functional-programming-parametricity-types"
      (Just "https://github.com/tonymorris/yow-west-2016/")
      [
        PresentationGiven
          "1d388b6263e7cbeedfbea224997648daa1d7862d"
          (BasePath "yow-west-2016/1d388b6263e7cbeedfbea224997648daa1d7862d/parametricity.pdf")
          mempty
          "YOW! West"
          "Perth, Australia"
          "2016-05-03"
      ]
  , Presentation
      "The Expression Problem and Lenses"
      "the-expression-problem-and-lenses"
      (Just "https://github.com/tonymorris/lambdajam-2016/")
      [
        PresentationGiven
          "9ae5e0fa2e1b0db7589dc5398bab4b07dc971e21"
          (BasePath "lambdajam-2016/9ae5e0fa2e1b0db7589dc5398bab4b07dc971e21/tep-lens.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=W6rE9Srx3N0")
          "Lambdajam"
          "Brisbane, Australia"
          "2016-04-28"
      ]
  , Presentation
      "The Essential Tools of Open-Source : Functional Programming, Parametricity, Types"
      "the-essential-tools-of-open-source-functional-programming-parametricity-types"
      (Just "https://github.com/tonymorris/linux.conf.au-2015/")
      [
        PresentationGiven
          "1d4d9a7bee05e8c14503b863272ddd6e3222a121"
          (BasePath "parametricity/1d4d9a7bee05e8c14503b863272ddd6e3222a121/parametricity.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=4hTtcUeqmfY")
          "linux.conf.au FP miniconf"
          "Geelong, Australia"
          "2016-02-02"
      ]
  , Presentation
      "Perhaps There is a Much Better Way"
      "perhaps-there-is-a-much-better-way"
      (Just "https://github.com/tonymorris/djangocon-2014/")
      [
        PresentationGiven
          "347452080349c2d15b28bf6e2dbc0d7d88c3a948"
          (BasePath "djangocon-2014/347452080349c2d15b28bf6e2dbc0d7d88c3a948/djangocon-2014.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=uqsZa36Io2M")
          "DjangoCon 2014 conference"
          "Brisbane, Australia"
          "2014-08-01"
      ]
  , Presentation
      "Scalaz: the history, the motivation, the battles, the future"
      "scalaz-the-history-the-motivation-the-battles-the-future"
      (Just "https://github.com/tonymorris/scalaz-history/")
      [
        PresentationGiven
          "a35d05af3cab0430f61f3d93b9d82ef9d0fc2ebe"
          (BasePath "scalaz-history/a35d05af3cab0430f61f3d93b9d82ef9d0fc2ebe/scalaz-history.pdf")
          mempty
          "LambdaJam 2014 conference"
          "Chicago, USA"
          "2014-07-23"
      ]
  , Presentation
      "Parametricity, Types are Documentation"
      "parametricity-types-are-documentation"
      (Just "https://github.com/tonymorris/parametricity/")
      [
        PresentationGiven
          "4985cb8e6d8d9a24e32d98204526c8e3b9319e33"
          (BasePath "parametricity/4985cb8e6d8d9a24e32d98204526c8e3b9319e33/parametricity.pdf")
          mempty
          "YOW! West conference"
          "Perth, Australia"
          "2014-05-13"
      , PresentationGiven
          "b8e0cb9d7f5dcdf48ef18eb49fc84905ece6b8ec"
          (BasePath "parametricity/b8e0cb9d7f5dcdf48ef18eb49fc84905ece6b8ec/parametricity.pdf")
          mempty
          "LambdaJam conference"
          "Brisbane, Australia"
          "2014-05-09"

      ]
  , Presentation
      "A Modern History of Lenses"
      "a-modern-history-of-lenses"
      Nothing
      [
        PresentationGiven
          "f1816a123cfc8d6f7cc6a586ca851da4c02452f7"
          (BasePath "modern-history-of-lenses/f1816a123cfc8d6f7cc6a586ca851da4c02452f7/lenses.pdf")
          mempty
          "LambdaJam conference"
          "Brisbane, Australia"
          "2014-05-09"
      ]
  , Presentation
      "Comonads, Applicative Functors, Monads and other principled things"
      "comonads-applicative-functors-monads-and-other-principled-things"
      (Just "https://github.com/tonymorris/comonads-applicative-monads/")
      [
        PresentationGiven
          "0030f8d270557db0f6d3daec0ec0a43496e579de"
          (BasePath "comonads-applicative-monads/0030f8d270557db0f6d3daec0ec0a43496e579de/comonads-applicative-monads.pdf")
          (videoLinkOnly1 "https://www.youtube.com/watch?v=R8yg6J9basE")
          "Codemania conference"
          "Auckland, New Zealand"
          "2014-04-04"
      , PresentationGiven
          "908fe5a4de2c22a2e8bc5fd3116d4dd7f2c85486"
          (BasePath "comonads-applicative-monads/908fe5a4de2c22a2e8bc5fd3116d4dd7f2c85486/comonads-applicative-monads.pdf")
          (videoLinkOnly1 "https://vimeo.com/90738761")
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2014-03-25"
      ]
  , Presentation
      "Zippers, Comonads & Data Structures in Scala"
      "zippers-comonads-data-structures-in-scala"
      Nothing
      [
        PresentationGiven
          "lambdajam-2013-06-21"
          (FullPath "https://mth.io/slides/zippers-and-comonads/?utm_content=buffer63a3f&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer#/")
          mempty
          "Lambdajam"
          "Brisbane, Australia"
          "2013-06-21"
      ]
  , Presentation
      "Explain List Folds to Yourself"
      "explain-list-folds-to-yourself"
      (Just "https://github.com/tonymorris/list-folds/")
      [
        PresentationGiven
          "b30aa0fdff296c731bc5b1c824adf1d02b3b69d9"
          (BasePath "list-folds/b30aa0fdff296c731bc5b1c824adf1d02b3b69d9/list-folds.pdf")
          (videoLinkOnly1 "https://vimeo.com/64673035")
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2013-04-23"
      ]
  , Presentation
      "What is FP (for Java)"
      "what-is-fp-for-java"
      Nothing
      [
        PresentationGiven
          "bfpg-2012-06-27"
          NoPath
          (videoLinkOnly1 "https://vimeo.com/44767789")
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2012-06-27"
      ]
  , Presentation
      "Dependency Injection Without the Gymnastics"
      "dependency-injection-without-the-gymnastics"
      Nothing
      [
        PresentationGiven
          "ete-2012-04-11"
          NoPath
          (videoLinkOnly1 "https://www.youtube.com/watch?v=ECPGTUa1WAI")
          "Emerging Technologies for the Enterprise"
          "Philadelphia, USA"
          "2012-04-11"
      ]
  , Presentation
      "What Does Functional Programming Mean?"
      "what-does-functional-programming-mean"
      (Just "https://github.com/tonymorris/what-does-fp-mean/")
      [
        PresentationGiven
          "bfpg-2010-03-24"
          (BasePath "what-does-fp-mean/what-does-fp-mean/chunk-html")
          (videoLinkOnly1 "https://vimeo.com/44767789")
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2010-03-24"
      ]
  , Presentation
      "Applicative Programming, Disjoint Unions, Semigroups and Non-breaking Error Handling"
      "applicative-programming-disjoint-unions-semigroups-and-non-breaking-error-handling"
      (Just "https://github.com/tonymorris/applicative-errors-scala")
      [
        PresentationGiven
          "bfpg-2010-04-01"
          (BasePath "applicative-errors-scala/")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2010-04-01"
      ]
  , Presentation
      "What Does Monad Mean?"
      "what-does-monad-mean"
      (Just "https://github.com/tonymorris/what-does-monad-mean/")
      [
        PresentationGiven
          "bfpg-2009-11-17"
          (BasePath "what-does-monad-mean/what-does-monad-mean/chunk-html")
          (videoLinkOnly1 "https://vimeo.com/8729673")
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2009-11-17"
      ]
  , Presentation
      "Introduction to High-level Programming with Scala"
      "introduction-to-high-level-programming-with-scala"
      (Just "https://github.com/tonymorris/intro-to-high-level-programming-scala")
      [
        PresentationGiven
          "bfpg-2008-07-28"
          (BasePath "intro-to-high-level-programming-scala/")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2008-07-28"
      ]
    , Presentation
      "Automated Testing"
      "automated-testing"
      Nothing
      [
        PresentationGiven
          "bfpg-2010-07-06"
          (BasePath "automated-testing")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2010-07-06"
      ]
  , Presentation
      "Dependency Injection Without the Gymnastics"
      "di-without-the-gymnastics"
      Nothing
      [
        PresentationGiven
          "di-without-the-gymnastics-2012-04-11"
          (BasePath "di-without-the-gymnastics")
          mempty
          "Philadelphia Emerging Technologies for the Enterprise"
          "Philadelphia, USA"
          "2012-04-11"
      ]
  , Presentation
      "Functional Programming for Business"
      "fp-for-business"
      (Just "https://github.com/tonymorris/fp-for-business")
      [
        PresentationGiven
          "ac8f4ca9fa1d65cf5d7f35d0548431e38e07767e"
          (BasePath "fp-for-business/ac8f4ca9fa1d65cf5d7f35d0548431e38e07767e/fp-for-business.pdf")
          mempty
          "NICTA Seminar Series"
          "Sydney, Australia"
          "2015"
      ]
  , Presentation
      "Advanced Programming Concepts using Haskell"
      "haskell-intro"
      Nothing
      [
        PresentationGiven
          "bfpg-haskell-intro-2009"
          (BasePath "haskell-intro")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2009"
      ]
  , Presentation
      "Monadic Parsers using Haskell"
      "haskell-parsers"
      Nothing
      [
        PresentationGiven
          "bfpg-haskell-parsers-2009"
          (BasePath "haskell-parsers")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2009"
      ]
  , Presentation
      "How to Learn Functional Programming"
      "how-to-learn-fp"
      Nothing
      [
        PresentationGiven
          "bfpg-2010-06-28"
          (BasePath "how-to-learn-fp")
          (videoLinkOnly1 "https://vimeo.com/13558699")
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2010-06-28"
      ]
  , Presentation
      "How do I convince my boss to use my favourite FP language?"
      "how-to-use-fp"
      Nothing
      [
        PresentationGiven
          "bfpg-how-to-use-fp-2012"
          (BasePath "how-to-use-fp/chunk-html/")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2012"
      ]
  , Presentation
      "Patterns: Reduction to the Inconsequential"
      "patterns"
      Nothing
      [
        PresentationGiven
          "qut-patterns-2008"
          (BasePath "patterns")
          mempty
          "Queensland University of Technology"
          "Brisbane, Australia"
          "2008"
      ]
  , Presentation
      "Using Lazy, Pure Programming for Navigating Remote Environments"
      "programming-for-remote-environments"
      Nothing
      [
        PresentationGiven
          "bfpg-programming-for-remote-environments-2010"
          (BasePath "programming-for-remote-environments")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2010"
      ]
  , Presentation
      "Pure Functional Programming Live"
      "purefp-live"
      Nothing
      [
        PresentationGiven
          "bfpg-purefp-live-2012"
          (BasePath "purefp-live")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2012"
      ]
  , Presentation
      "Configuration Without the Bugs and Gymnastics"
      "reader-monad"
      Nothing
      [
        PresentationGiven
          "bfpg-reader-monad-2012"
          (BasePath "reader-monad")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2012"
      ]
  , Presentation
      "Logging Without Side-effects"
      "writer-monad"
      Nothing
      [
        PresentationGiven
          "bfpg-writer-monad-2011"
          (BasePath "writer-monad")
          mempty
          "Brisbane Functional Programming Group"
          "Brisbane, Australia"
          "2011"
      ]
  ]

renderSoftwarePresentation ::
  [Presentation]
  -> [Documents DocumentWithNameUri]
renderSoftwarePresentation pts =
  let renderSource x =
        if "https://github.com" `isPrefixOf` x
        then
          "Source (github)"
        else
          "Source"
      renderSlides x =
        if ".pdf" `isSuffixOf` x
        then
          "Slides (pdf)"
        else
          "Slides"
      renderVideo x n =
        let renderVideo'
              | "https://www.youtube.com" `isPrefixOf` x = "Video (youtube)"
              | "https://vimeo.com" `isPrefixOf` x = "Video (vimeo)"
              | otherwise = "Video"
            n' =
              maybe "" (" " <>) n
        in  renderVideo' <> n'
      perPresentation ::
        Presentation
        -> [(String, String, String, [DocumentWithNameUri])]
      perPresentation (Presentation nm i src ps) =
        let perPresentationGiven ::
              PresentationGiven
              -> (String, String, String, [DocumentWithNameUri])
            perPresentationGiven (PresentationGiven pid sp urls pnm loc dt) =
              (
                dt
              , nm <> " &#x2218; " <> pnm <> ", " <> loc <> " &#x2218; " <> dt
              , i <> "-" <> pid
              , fmap (\src' -> DocumentWithNameUri (renderSource src') src') (toListOf _Just src)
                <>
                fmap (\sp' -> DocumentWithNameUri (renderSlides sp') sp') (toListOf _Just (slidesPathToMaybe sp))
                <>
                fmap (\(url', n) -> DocumentWithNameUri (renderVideo url' n) url') urls
              )
        in  fmap perPresentationGiven ps
      sorter (dt1, _, _, _) (dt2, _, _, _) =
        dt2 `compare` dt1
  in  fmap (\(_, b, c, d) -> Documents b c d) . sortBy sorter $ pts >>= perPresentation

presentations ::
  [Documents DocumentWithNameUri]
presentations =
  htmlEscapeDocumentsList $
  aviationBriefingSlides <>
  renderSoftwarePresentation softwarePresentations
