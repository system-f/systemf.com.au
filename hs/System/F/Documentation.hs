{-# OPTIONS_GHC -Wall #-}

module System.F.Documentation(
  module S
, documentation
) where

import Control.Lens
import System.F.Documentation.AircraftChecklist as S
import System.F.Documentation.AircraftManual as S
import System.F.Documentation.Archerfield as S
import System.F.Documentation.AirLaw as S
import System.F.Documentation.CASA as S
import System.F.Documentation.Data as S
import System.F.Documentation.Heckfield as S
import System.F.Documentation.Miscellaneous as S
import System.F.Documentation.Presentations as S
import System.F.Documentation.RAAus as S
import System.F.Documentation.Software as S
import System.F.Documentation.Util as S

documentation ::
  [DocumentWithNameUri]
documentation =
    over (mapped . documentUri)
    (\z -> "/p/documentation/" <> z <> "/")
    [

      DocumentWithNameUri "Aircraft Manuals" "aircraft/manual"
    , DocumentWithNameUri "Aircraft Checklists" "aircraft/checklist"
    , DocumentWithNameUri "Air Law" "air-law"
    , DocumentWithNameUri "CASA" "casa"
    , DocumentWithNameUri "RAAus" "raaus"
    , DocumentWithNameUri "Archerfield" "archerfield"
    , DocumentWithNameUri "Heckfield" "heckfield"
    , DocumentWithNameUri "Miscellaneous" "miscellaneous"
    ]
