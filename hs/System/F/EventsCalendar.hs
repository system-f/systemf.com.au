{-# OPTIONS_GHC -Wall #-}

module System.F.EventsCalendar where

eventsCalendars ::
  [(String, String)]
eventsCalendars =
  [
    ("79105aa3372eea04d0e8fc58b50b32b674a0e32519906ed994588817e0c0eb88", "Fly-ins")
  , ("f5e56a35d2abedc4c378a1ddfc17e7f23430154cf0ad2feb0e6bc9bf2cb6194a", "Air Shows")
  ]

eventsCalendarUrl ::
  [(String, String)]
  -> String
eventsCalendarUrl es =
  "https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;ctz=Australia%2FBrisbane&amp;"
  <>
  (
    es >>= \(u, _) ->
      "src="
      <>
      u
      <>
      "%40group.calendar.google.com&amp;"
  )
  <>
  "color=%23AD1457&amp;showTitle=0&amp;showNav=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=1&amp;mode=AGENDA&amp;hl=en_GB"

eventsCalendarUrl' ::
  String
eventsCalendarUrl' =
  eventsCalendarUrl eventsCalendars
