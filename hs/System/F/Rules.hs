{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module System.F.Rules( systemfRules ) where

import Control.Applicative( liftA2 )
import Hakyll
import System.F.Configuration
import System.F.Documentation
import System.F.EventsCalendar

redirect ::
  Identifier
  -> String
  -> Rules ()
redirect fr to =
  create [fr] $
    do  route idRoute
        compile . makeItem . Redirect $ to

systemfRules ::
  Rules ()
systemfRules =
  do
      match "robots.txt" $ do
        route idRoute
        compile copyFileCompiler

      match "google48f6b5ec29d50150.html" $ do
        route idRoute
        compile copyFileCompiler

      match "images/**" $ do
        route idRoute
        compile copyFileCompiler

      match "modules/**" $ do
        route idRoute
        compile copyFileCompiler

      match "aip-version/**" $ do
        route idRoute
        compile copyFileCompiler

      match "n-pages/**" $ do
        route idRoute
        compile copyFileCompiler

      match "linkcheckerrc" $ do
        route idRoute
        compile copyFileCompiler

      match "css/*" $ do
        route idRoute
        compile compressCssCompiler

      match "images/**" $ do
        route idRoute
        compile compressCssCompiler

      match "404.html" $ do
        route $ setExtension "html"
        compile $
          pandocCompiler
            >>= loadAndApplyTemplate "templates/systemf.html" context

      match "index.html" $ do
        route $ setExtension "html"
        compile $
          pandocCompiler
            >>= loadAndApplyTemplate "templates/systemf.html" context
            >>= relativizeUrls

      (ersa, charts) <-
        preprocess $
          liftA2 (,) (readFile "aip-version/ersa/current") (readFile "aip-version/charts/current")

      match "p/software/practice/**" $ do
        route $ setExtension "html"
        compile $ do
          getResourceBody
            >>= applyAsTemplate context
            >>= loadAndApplyTemplate "templates/software/practice.html" context
            >>= loadAndApplyTemplate "templates/systemf.html" context
            >>= relativizeUrls

      match "p/**" $ do
        route $ setExtension "html"
        compile $ do
          let pageContext =
                openGraphField "opengraph" context
                <> twitterCardField "twitter" context
                <> context
                <> constField "ersa-current" ersa
                <> constField "charts-current" charts
          getResourceBody
            >>= applyAsTemplate pageContext
            >>= loadAndApplyTemplate "templates/systemf.html" pageContext
            >>= relativizeUrls

      match "templates/**" $ compile templateCompiler

      airLaw'' <- preprocess airLaw
      documentationPage'
        "p/documentation/air-law/"
        (documentationAccordionPage (documentPartsContextNPages <> documentPartImageContext) . Documents "Air Law" "air-law")
        (documentationNoAccordianPage (documentPartsContextNPages <> documentPartImageContext))
        airLaw''

      aircraftManual'' <- preprocess aircraftManual
      documentationPage'
        "p/documentation/aircraft/manual/"
        (documentationAccordionPage (documentPartsContextNPages <> documentPartImageContext) . Documents "Aircraft Manual" "aircraft-manual")
        (documentationNoAccordianPage (documentPartsContextNPages <> documentPartImageContext))
        aircraftManual''

      create ["p/documentation/aircraft/checklist/index.html"] $
        documentationNoAccordianPage documentPartsContext
        aircraftChecklist

      casa'' <- preprocess casa
      documentationPage'
        "p/documentation/casa/"
        (documentationAccordionPage (documentPartsContextNPages <> documentPartImageContext) . Documents "CASA" "casa")
        (documentationNoAccordianPage (documentPartsContextNPages <> documentPartImageContext))
        casa''

      raaus'' <- preprocess raaus
      documentationPage'
        "p/documentation/raaus/"
        (documentationAccordionPage (documentPartsContextNPages <> documentPartImageContext) . Documents "Recreational Aviation Australia (RAAus)" "raaus")
        (documentationNoAccordianPage (documentPartsContextNPages <> documentPartImageContext))
        raaus''

      documentationPage'
        "p/documentation/archerfield/"
        (documentationAccordionPage (documentPartsContext <> documentPartImageContext) . Documents "Archerfield" "archerfield")
        (documentationNoAccordianPage (documentPartsContext <> documentPartImageContext))
        (archerfield ersa charts)

      documentationPage'
        "p/documentation/heckfield/"
        (documentationAccordionPage (documentPartsContext <> documentPartImageContext) . Documents "Heckfield" "heckfield")
        (documentationNoAccordianPage (documentPartsContext <> documentPartImageContext))
        (heckfield ersa charts)

      create ["p/documentation/miscellaneous/index.html"] $
        documentationNoAccordianPage (documentPartsContext <> documentPartImageContext)
        miscellaneous

      -- old link 2024-11-14
      redirect "p/slides/index.html" "/p/presentations/"

      documentationPage'
        "p/presentations/"
        (documentationAccordionPage documentPartsContext . Documents "Presentations" "presentations" )
        (documentationNoAccordianPage documentPartsContext)
        presentations

      documentationPage'
        "p/software/"
        (documentationAccordionPage documentPartsContext . Documents "Software" "software")
        (documentationNoAccordianPage documentPartsContext)
        software

      create ["p/documentation/index.html"] $
        (documentationNoAccordianPage documentPartsContext . Documents "Documentation" "documentation")
        documentation

      create ["p/events/index.html"] $
        let calendarsContext =
              field "calendar-id" (pure . view _1 . itemBody)
              <>
              field "calendar-name" (pure . view _2 . itemBody)
            pageContext =
              context
              <>
              constField "title" "Events"
              <>
              constField "did" "events"
              <>
              constField "events-calendar-url" eventsCalendarUrl'
              <>
              listField "calendars" calendarsContext (makeItems eventsCalendars)
        in  do  route idRoute
                compile $ do
                  makeItem ("" :: String)
                    >>= loadAndApplyTemplate "templates/events.html" pageContext
                    >>= loadAndApplyTemplate "templates/systemf.html" pageContext
                    >>= relativizeUrls
      redirect "elp/index.html" "/p/elp/"
      redirect "presentations/index.html" "/p/presentations/"
      redirect "slides/index.html" "/p/presentations/"
      redirect "software/index.html" "/p/software/"
      redirect "events/index.html" "/p/events/"

documentPartsContext ::
  (HasDocumentUri s, HasDocumentName s) =>
  Context s
documentPartsContext =
  field "document-part-uri" (pure . view documentUri . itemBody)
  <>
  field "document-part-name" (pure . view documentName . itemBody)

documentPartsContextNPages ::
  (HasDocumentUri s, HasDocumentName s, HasDocumentNPages s) =>
  Context s
documentPartsContextNPages =
  documentPartsContext
  <>
  field "document-part-n-pages" (pure . view documentNPages . itemBody)

documentPartImageContext ::
  HasDocumentImage s =>
  Context s
documentPartImageContext =
  field "document-part-uri-img" (pure . view documentImage . itemBody)

documentationAccordionPage ::
  (HasDocumentsName s, HasDocumentsId s, HasDocumentsValues s a) =>
  Context a
  -> Documents s
  -> Rules ()
documentationAccordionPage ctx =
  let outerContext =
        field "document-name" (pure . view documentsName . itemBody)
        <>
        field "document-id" (pure . view documentsId . itemBody)
        <>
        listFieldWith "inner" ctx (makeItems . view documentsValues . itemBody)
  in  documentationPageWithTemplate outerContext (loadAndApplyTemplate "templates/documentation/accordion.html")

documentationNoAccordianPage ::
  Context s
  -> Documents s
  -> Rules ()
documentationNoAccordianPage ctx =
  documentationPageWithTemplate
    ctx
    (loadAndApplyTemplate "templates/documentation/no-accordion.html")

documentationPageWithTemplate ::
  Context s
  -> (Context String -> Item String -> Compiler (Item String))
  -> Documents s
  -> Rules ()
documentationPageWithTemplate ctx templ ds =
  do  route idRoute
      compile $ do
        let pageContext =
              listField "items" ctx (makeItems (view documentsValues ds))
              <>
              context
              <>
              constField "title" (view documentsName ds)
              <>
              constField "did"  (view documentsId ds)
        makeItem ("" :: String)
          >>= templ pageContext
          >>= loadAndApplyTemplate "templates/systemf.html" pageContext
          >>= relativizeUrls

documentationPage' ::
  (Foldable t, HasDocumentsId s) =>
  FilePath
  -> (t s -> Rules ())
  -> (s -> Rules ())
  -> t s
  -> Rules ()
documentationPage' doc ix f docs =
  do  create [fromFilePath (doc <> "index.html")] (ix docs)
      mapM_ (\r -> create [fromFilePath (doc <> view documentsId r <> "/index.html")] $ f r) docs

makeItems ::
  Traversable t =>
  t a
  -> Compiler (t (Item a))
makeItems =
  traverse makeItem

context ::
  Context String
context =
  mapContext escapeHtml metadataField
  <>
  dateField "date" "%Y-%m-%d"
  <>
  snapshotField "title" "title"
  <>
  constField "siteTitle" siteTitle
  <>
  constField "systemfEmail" systemfEmail
  <>
  constField "systemfPhone" systemfPhone
  <>
  constField "systemfGitlabio" systemfGitlabio
  <>
  snapshotField "og-description" "abstract"
  <>
  -- constField "og-image" "image.jpg"
  -- <>
  -- constField "twitter-creator" "@user"
  -- <>
  bodyField "body"
  <>
  urlField "url"
  <>
  pathField "path"
  <>
  constField "root" siteRoot

-- | Get field content from snapshot (at item version "recent")
snapshotField ::
  String           -- ^ Key to use
  -> Snapshot         -- ^ Snapshot to load
  -> Context String   -- ^ Resulting context
snapshotField key snap =
  field key $ \item ->
    loadSnapshotBody (setVersion (Just "recent") (itemIdentifier item)) snap
