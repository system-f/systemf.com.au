{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module System.F.Pandoc(
  removeFormatting
, firstHeader
, firstHeaderContents
, abstract
, addSectionLinks
) where

import Control.Applicative ( Alternative((<|>)) )
import Control.Lens ( Traversal' )
import Data.Monoid ( First(First, getFirst) )
import Text.Pandoc.Definition
    ( Inline(Link, Span, RawInline, Math, LineBreak, SoftBreak, Space,
             Code, Str),
      Pandoc(..),
      Block(Header, Para) )
import Text.Pandoc.Walk (query, walk)

-- yield "plain" terminal inline content; discard formatting
removeFormatting ::
  [Inline]
  -> [Inline]
removeFormatting =
  query
    (\case
      Str s ->
        [Str s]
      Code _ s ->
        [Str s]
      Space ->
        [Space]
      SoftBreak ->
        [Space]
      LineBreak ->
        [LineBreak]
      Math _ s ->
        [Str s]
      RawInline _ s ->
        [Str s]
      _ ->
        []
    )

-- | Extract the abstract, or autogenerate one (badly).
--
-- Looks for a Span with class "abstract".  If not found,
-- takes the first paragraph that immediately precedes a
-- header.  Strips all formatting.
--
abstract ::
  Pandoc
  -> Maybe [Inline]
abstract (Pandoc _ blocks) =
  let markedUp = fmap getFirst . query $ \case
        Span (_id, cls, _attrs) inls | "abstract" `elem` cls -> First (Just inls)
        _ -> mempty
      fallback (Para inlines : Header {} : _) = Just inlines
      fallback (_h : t) = fallback t
      fallback [] = Nothing
  in  removeFormatting <$> (markedUp blocks <|> fallback blocks)

firstHeader ::
  Pandoc
  -> Maybe [Inline]
firstHeader (Pandoc _ xs) =
  let go [] =
        Nothing
      go (Header _ _ ys : _) =
        Just ys
      go (_ : t) =
        go t
  in  go xs

firstHeaderContents ::
  Traversal' Pandoc [Inline]
firstHeaderContents _ (Pandoc m []) =
  pure (Pandoc m [])
firstHeaderContents f (Pandoc m (Header n a y : r)) =
  fmap (\y' -> Pandoc m (Header n a y' : r)) (f y)
firstHeaderContents f (Pandoc m (q : r)) =
  fmap (\(Pandoc m' r') -> Pandoc m' (q:r')) (firstHeaderContents f (Pandoc m r))

addSectionLinks ::
  Pandoc
  -> Pandoc
addSectionLinks =
  let f (Header n attr@(idAttr, _, _) inlines) | n > 1 =
          let link = Link ("", ["section"], []) [Str "§"] ("#" <> idAttr, "")
          in Header n attr (inlines <> [Space, link])
      f x = x
  in  walk f
