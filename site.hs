{-# OPTIONS_GHC -Wall #-}

import System.F.Configuration ( systemfConfiguration )
import System.F.Rules ( systemfRules )
import Hakyll ( hakyllWith )

main ::
  IO ()
main =
  hakyllWith systemfConfiguration systemfRules
