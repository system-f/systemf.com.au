#!/bin/bash

script_dir=$(dirname "$0")
dist_dir="${script_dir}/../public"

linkchecker --check-extern --config=${dist_dir}/linkcheckerrc ${dist_dir}

