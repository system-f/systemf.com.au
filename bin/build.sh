#!/bin/bash

set -e

script_dir=$(dirname "$0")

${script_dir}/aip-version.sh
${script_dir}/n-pages.sh
cabal run site -- build
