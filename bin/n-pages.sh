#!/bin/bash

set -e

script_dir=$(dirname "$0")
n_pages_dir="${script_dir}/../n-pages"
n_pages_tar_gz="npages.tar.gz"

mkdir -p "${n_pages_dir}"

download_aircraft_manual_n_pages() {
  if [ -z "$1" ]
  then
    >&2 echo "Require one (1) argument <aircraft-manual-repository>" >&2
  else
    mkdir -p "${n_pages_dir}/aircraft/manual/$1"
    wget -O - "https://system-f.gitlab.io/aircraft/manual/$1/npages.tar.gz" > "${n_pages_dir}/aircraft/manual/$1/npages.tar.gz"
    pushd "${n_pages_dir}/aircraft/manual/$1"
    tar -zxvf "${n_pages_tar_gz}"
    popd
  fi
}

download_aircraft_manual() {
  download_aircraft_manual_n_pages "sling-2"
  download_aircraft_manual_n_pages "eurofox-3k"
  download_aircraft_manual_n_pages "cessna-152"
  download_aircraft_manual_n_pages "cessna-172r"
  download_aircraft_manual_n_pages "cessna-172s"
  download_aircraft_manual_n_pages "cessna-182p"
  download_aircraft_manual_n_pages "aquila-a210"
  download_aircraft_manual_n_pages "vans-rv14"
  download_aircraft_manual_n_pages "rotax-912"
  download_aircraft_manual_n_pages "dynon"
  download_aircraft_manual_n_pages "garmin"
  download_aircraft_manual_n_pages "avmap"
}

download_air_law() {
  mkdir -p "${n_pages_dir}/air-law"
  wget -O - "https://air-law.systemf.com.au/npages.tar.gz" > "${n_pages_dir}/air-law/npages.tar.gz"
  pushd "${n_pages_dir}/air-law"
  tar -zxvf "${n_pages_tar_gz}"
  popd
}

download_casa_documentation() {
  mkdir -p "${n_pages_dir}/casa-documentation"
  wget -O - "https://casa-documentation.systemf.com.au/npages.tar.gz" > "${n_pages_dir}/casa-documentation/npages.tar.gz"
  pushd "${n_pages_dir}/casa-documentation"
  tar -zxvf "${n_pages_tar_gz}"
  popd
}

download_airport_documentation() {
  mkdir -p "${n_pages_dir}/airport-documentation"
  wget -O - "https://airport-documentation.systemf.com.au/npages.tar.gz" > "${n_pages_dir}/airport-documentation/npages.tar.gz"
  pushd "${n_pages_dir}/airport-documentation"
  tar -zxvf "${n_pages_tar_gz}"
  popd
}

download_raaus_documentation() {
  mkdir -p "${n_pages_dir}/raaus-documentation"
  wget -O - "https://raaus-documentation.systemf.com.au/npages.tar.gz" > "${n_pages_dir}/raaus-documentation/npages.tar.gz"
  pushd "${n_pages_dir}/raaus-documentation"
  tar -zxvf "${n_pages_tar_gz}"
  popd
}

download_aircraft_manual
download_air_law
download_casa_documentation
download_airport_documentation
download_raaus_documentation
