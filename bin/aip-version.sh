#!/bin/bash

set -e

script_dir=$(dirname "$0")
aip_version_dir="${script_dir}/../aip-version"

mkdir -p "${aip_version_dir}/ersa"
mkdir -p "${aip_version_dir}/charts"

wget -O - "https://aip-version.systemf.com.au/ersa/current" > "${aip_version_dir}/ersa/current"
wget -O - "https://aip-version.systemf.com.au/charts/current" > "${aip_version_dir}/charts/current"
